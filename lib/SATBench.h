/* 
 * File:   SATBench.h
 * Author: zacknewsham
 *
 * Created on September 7, 2014, 9:39 PM
 */

#ifndef SATBENCH_H
#define	SATBENCH_H
#define API_HOST "satbench.dev"
#define API_PAGE "/api/0.2"
int SATBench_numwarnings;
char** SATBench_warnings;
int SATBench_error;

struct SATBench_formula{
  int     formula_id;
  char    formula_name[256];
  int     simplified;
  int     file_exists;
  unsigned char sha256[32];
  char**  keys;
  char**  values;
  int     length;
};

struct SATBench_worker{
  int     worker_id;
  char    worker_name[256];
  int     cpu_speed;
  char    cpu_arch[17];
  char    cpu_manufacturer[129];
  char    cpu_model[129];
  int     cpu_cores;
  int     l1_i_kbytes;
  int     l1_d_kbytes;
  int     l2_kbytes;
  int     l3_kbytes;
  int     ram_kbytes;
  double  ram_speed;
  int     ram_channels;
  int     page_kbytes;
  int     storage_speed;
  int     storage_connection_mbytes;
};
struct SATBench_solver{
  int     solver_id;
  char     solver_name[256];
  char   version[256];
  unsigned char   sha256[32];
};

struct SATBench_command{
  int     command_id;
  int     solver_id;
  int     simplification;
  int     threads;
  int     output;
  char**  keys;
  char**  values;
  int     length;
} ;

struct SATBench_trial{
  int     trial_id;
  int     command_id;
  int     formula_id;
  int     worker_id;
  int     wc_time_millis;
  int     cpu_time_millis;
  int     result;
  int     major_pagefaults;
  int     minor_pagefaults;
  int     inputs;
  int     outputs;
  int     swaps;
  int     max_resident;
} ;

int SATBench_register_formula(char* apikey, struct SATBench_formula * instance);
int SATBench_register_worker(char* apikey, struct SATBench_worker * worker);
int SATBench_register_solver(char* apikey, struct SATBench_solver * solver);
int SATBench_register_command(char* apikey, struct SATBench_command * command);
int SATBench_register_trial(char* apikey, struct  SATBench_trial * trial);
int SATBench_easy(/*...TODO...*/);
#endif	/* SATBENCH_H */

