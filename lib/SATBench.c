#include "SATBench.h"
#include "cJSON.h"

#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <netdb.h>
#include <curl/curl.h>
#include <netinet/tcp.h>
typedef struct api_request{
  char method_name[20];
  char apikey[128];
  union {
    struct SATBench_formula* formula;
    struct SATBench_worker* worker;
    struct SATBench_solver* solver;
    struct SATBench_command* command;
    struct SATBench_trial* trial;
  };
} api_request;

char hex_lookup[] = {0,1,2,3,4,5,6,7,8,9,'a', 'b', 'c','d','e','f'};
void populate_sha256_from_hex(char* sha256, char* hex){
  int i_counter,a_counter;
  for(i_counter = 0; i_counter < 64; i_counter += 2){
    char val = 0;
    for(a_counter = 0; a_counter < 16; a_counter++){
      if(hex_lookup[a_counter] == hex[i_counter]){
        val = 16 * a_counter;
      }
    }
    
    for(a_counter = 0; a_counter < 16; a_counter++){
      if(hex_lookup[a_counter] == hex[i_counter + 1]){
        val += a_counter;
      }
    }
    sha256[i_counter % 2] = val;
  }
}
int populate_formula_json(cJSON* object, struct SATBench_formula *formula){
  int i_counter = 0;
  formula->formula_id = cJSON_GetObjectItem(object, "formula_id")->valueint;
  formula->simplified = cJSON_GetObjectItem(object, "simplified")->valueint;
  strcpy(formula->formula_name,cJSON_GetObjectItem(object, "formula_name")->valuestring);
  SATBench_numwarnings = cJSON_GetObjectItem(object,"numwarnings")->valueint;
  SATBench_warnings = calloc(SATBench_numwarnings, sizeof(char*));
  for(i_counter = 0; i_counter < SATBench_numwarnings; i_counter++){
    cJSON * warning = cJSON_GetArrayItem(cJSON_GetObjectItem(object, "warnings"), i_counter);
    SATBench_warnings[i_counter] = malloc(sizeof(char) * strlen(warning->valuestring) + 1);
    strcpy(SATBench_warnings[i_counter],warning->valuestring);
  }
  populate_sha256_from_hex(formula->sha256, cJSON_GetObjectItem(object, "sha256")->valuestring);
  return 0;
}

int populate_json_formula(cJSON* object, struct SATBench_formula *formula){
  char sum[65];
  int i_counter;
  for(i_counter = 0; i_counter < 32; i_counter++){
    sprintf(sum + i_counter*2, "%02x ", formula->sha256[i_counter]);
  }
  sum[64] = 0;
  cJSON_AddItemToObject(object, "formula_name", cJSON_CreateString(formula->formula_name));
  cJSON_AddItemToObject(object, "sha256", cJSON_CreateString(sum));
  cJSON_AddItemToObject(object, "simplified", cJSON_CreateBool(formula->simplified));
  cJSON * details = cJSON_CreateObject();
  char* key;
  char* value;
  for(i_counter = 0; i_counter < formula->length; i_counter++){
    key=*formula->keys + (32*i_counter);
    value=*formula->values + (128*i_counter);
    cJSON_AddItemToObject(details, formula->keys[i_counter], cJSON_CreateString(formula->values[i_counter]));
  }
  cJSON_AddItemToObject(object, "details", details);
  
  return 0;
}

int populate_solver_json(cJSON* object,  struct SATBench_solver *solver){
  solver->solver_id = cJSON_GetObjectItem(object, "solver_id")->valueint;
  strcpy(solver->version,cJSON_GetObjectItem(object, "version")->valuestring);
  strcpy(solver->solver_name,cJSON_GetObjectItem(object, "solver_name")->valuestring);
  populate_sha256_from_hex(solver->sha256, cJSON_GetObjectItem(object, "sha256")->valuestring);
  return 0;
}

int populate_json_solver(cJSON* object, struct SATBench_solver *solver){
  char sum[65];
  int i_counter;
  for(i_counter = 0; i_counter < 32; i_counter++){
    sprintf(sum + i_counter*2, "%02x ", solver->sha256[i_counter]);
  }
  sum[64] = 0;
  cJSON_AddItemToObject(object, "solver_name", cJSON_CreateString(solver->solver_name));
  cJSON_AddItemToObject(object, "sha256", cJSON_CreateString(sum));
  cJSON_AddItemToObject(object, "version", cJSON_CreateString(solver->version));
  
  return 0;
}


int populate_worker_json(cJSON* object, struct SATBench_worker *worker){
  
  int i_counter = 0;
  worker->worker_id = cJSON_GetObjectItem(object, "worker_id")->valueint;
  strcpy(worker->worker_name,cJSON_GetObjectItem(object, "worker_name")->valuestring);
  strcpy(worker->cpu_arch,cJSON_GetObjectItem(object, "cpu_arch")->valuestring);
  strcpy(worker->cpu_manufacturer,cJSON_GetObjectItem(object, "cpu_manufacturer")->valuestring);
  strcpy(worker->cpu_model,cJSON_GetObjectItem(object, "cpu_model")->valuestring);
  worker->cpu_speed = cJSON_GetObjectItem(object, "cpu_speed")->valueint;
  worker->cpu_cores = cJSON_GetObjectItem(object, "cpu_cores")->valueint;
  worker->l1_i_kbytes = cJSON_GetObjectItem(object, "l1_i_kbytes")->valueint;
  worker->l1_d_kbytes = cJSON_GetObjectItem(object, "l1_d_kbytes")->valueint;
  worker->l2_kbytes = cJSON_GetObjectItem(object, "l2_kbytes")->valueint;
  worker->l3_kbytes = cJSON_GetObjectItem(object, "l3_kbytes")->valueint;
  worker->ram_kbytes = cJSON_GetObjectItem(object, "ram_kbytes")->valueint;
  worker->ram_speed = cJSON_GetObjectItem(object, "ram_speed")->valuedouble;
  worker->ram_channels = cJSON_GetObjectItem(object, "ram_channels")->valueint;
  worker->page_kbytes = cJSON_GetObjectItem(object, "page_kbytes")->valueint;
  worker->storage_speed = cJSON_GetObjectItem(object, "storage_speed")->valueint;
  worker->storage_connection_mbytes = cJSON_GetObjectItem(object, "storage_connection_mbytes")->valueint;
  
  SATBench_numwarnings = cJSON_GetObjectItem(object,"numwarnings")->valueint;
  SATBench_warnings = calloc(SATBench_numwarnings, sizeof(char*));
  for(i_counter = 0; i_counter < SATBench_numwarnings; i_counter++){
    cJSON * warning = cJSON_GetArrayItem(cJSON_GetObjectItem(object, "warnings"), i_counter);
    SATBench_warnings[i_counter] = malloc(sizeof(char) * strlen(warning->valuestring) + 1);
    strcpy(SATBench_warnings[i_counter],warning->valuestring);
  }
  
  return 0;
}

int populate_json_worker(cJSON* object, struct SATBench_worker *worker){
  cJSON_AddItemToObject(object, "worker_name", cJSON_CreateString(worker->worker_name));
  cJSON_AddItemToObject(object, "cpu_arch", cJSON_CreateString(worker->cpu_arch));
  cJSON_AddItemToObject(object, "cpu_manufacturer", cJSON_CreateString(worker->cpu_manufacturer));
  cJSON_AddItemToObject(object, "cpu_model", cJSON_CreateString(worker->cpu_model));
  cJSON_AddItemToObject(object, "cpu_speed", cJSON_CreateNumber((double)worker->cpu_speed));
  cJSON_AddItemToObject(object, "cpu_cores", cJSON_CreateNumber((double)worker->cpu_cores));
  cJSON_AddItemToObject(object, "l1_i_kbytes", cJSON_CreateNumber((double)worker->l1_i_kbytes));
  cJSON_AddItemToObject(object, "l1_d_kbytes", cJSON_CreateNumber((double)worker->l1_d_kbytes));
  cJSON_AddItemToObject(object, "l2_kbytes", cJSON_CreateNumber((double)worker->l2_kbytes));
  cJSON_AddItemToObject(object, "l3_kbytes", cJSON_CreateNumber((double)worker->l3_kbytes));
  cJSON_AddItemToObject(object, "ram_kbytes", cJSON_CreateNumber((double)worker->ram_kbytes));
  cJSON_AddItemToObject(object, "ram_speed", cJSON_CreateNumber((double)worker->ram_speed));
  cJSON_AddItemToObject(object, "ram_channels", cJSON_CreateNumber((double)worker->ram_channels));
  cJSON_AddItemToObject(object, "page_kbytes", cJSON_CreateNumber((double)worker->page_kbytes));
  cJSON_AddItemToObject(object, "storage_speed", cJSON_CreateNumber((double)worker->storage_speed));
  cJSON_AddItemToObject(object, "storage_connection_mbytes", cJSON_CreateNumber((double)worker->storage_connection_mbytes));
  return 0;
}


int populate_trial_json(cJSON* object, struct SATBench_trial *trial){
  trial->trial_id = cJSON_GetObjectItem(object, "trial_id")->valueint;
  trial->command_id = cJSON_GetObjectItem(object, "command_id")->valueint;
  trial->formula_id = cJSON_GetObjectItem(object, "formula_id")->valueint;
  trial->worker_id = cJSON_GetObjectItem(object, "worker_id")->valueint;
  trial->wc_time_millis = cJSON_GetObjectItem(object, "wc_time_millis")->valueint;
  trial->cpu_time_millis = cJSON_GetObjectItem(object, "cpu_time_millis")->valueint;
  trial->result = cJSON_GetObjectItem(object, "result")->valueint;
  trial->major_pagefaults = cJSON_GetObjectItem(object, "major_pagefaults")->valueint;
  trial->minor_pagefaults = cJSON_GetObjectItem(object, "minor_pagefaults")->valueint;
  trial->inputs = cJSON_GetObjectItem(object, "inputs")->valueint;
  trial->outputs = cJSON_GetObjectItem(object, "outputs")->valueint;
  trial->max_resident = cJSON_GetObjectItem(object, "max_resident")->valueint;
  trial->swaps = cJSON_GetObjectItem(object, "swaps")->valueint;
  return 0;
}

int populate_json_trial(cJSON* object, struct SATBench_trial *trial){
  cJSON_AddItemToObject(object, "command_id", cJSON_CreateNumber(trial->command_id));
  cJSON_AddItemToObject(object, "formula_id", cJSON_CreateNumber(trial->formula_id));
  cJSON_AddItemToObject(object, "worker_id", cJSON_CreateNumber(trial->worker_id));
  cJSON_AddItemToObject(object, "wc_time_millis", cJSON_CreateNumber(trial->wc_time_millis));
  cJSON_AddItemToObject(object, "cpu_time_millis", cJSON_CreateNumber(trial->cpu_time_millis));
  cJSON_AddItemToObject(object, "result", cJSON_CreateNumber(trial->result));
  cJSON_AddItemToObject(object, "major_pagefaults", cJSON_CreateNumber(trial->major_pagefaults));
  cJSON_AddItemToObject(object, "minor_pagefaults", cJSON_CreateNumber(trial->minor_pagefaults));
  cJSON_AddItemToObject(object, "inputs", cJSON_CreateNumber(trial->inputs));
  cJSON_AddItemToObject(object, "outputs", cJSON_CreateNumber(trial->outputs));
  cJSON_AddItemToObject(object, "max_resident", cJSON_CreateNumber(trial->max_resident));
  cJSON_AddItemToObject(object, "swaps", cJSON_CreateNumber(trial->swaps));
  return 0;
}

int populate_command_json(cJSON* object, struct SATBench_command *command){
  command->command_id = cJSON_GetObjectItem(object, "command_id")->valueint;
  command->solver_id = cJSON_GetObjectItem(object, "solver_id")->valueint;
  command->output = cJSON_GetObjectItem(object, "output")->valueint;
  command->simplification = cJSON_GetObjectItem(object, "simplification")->valueint;
  command->threads = cJSON_GetObjectItem(object, "threads")->valueint;
  return 0;
}

int populate_json_command(cJSON* object, struct SATBench_command *command){
  cJSON_AddItemToObject(object, "solver_id", cJSON_CreateNumber(command->solver_id));
  cJSON_AddItemToObject(object, "output", cJSON_CreateBool(command->output));
  cJSON_AddItemToObject(object, "simplification", cJSON_CreateBool(command->simplification));
  cJSON_AddItemToObject(object, "threads", cJSON_CreateNumber(command->threads));
  //cJSON_AddItemToObject(object, "args", cJSON_CreateString(command->args));
  cJSON * details = cJSON_CreateObject();
  char* key;
  char* value;
  int i_counter;
  for(i_counter = 0; i_counter < command->length; i_counter++){
    key=*command->keys + (32*i_counter);
    value=*command->values + (128*i_counter);
    cJSON_AddItemToObject(details, command->keys[i_counter], cJSON_CreateString(command->values[i_counter]));
  }
  cJSON_AddItemToObject(object, "args", details);
  return 0;
}

char* get_body(char* response){
  int i_counter = 0;
  
  while(response[i_counter] != '\r' || response[i_counter + 1] != '\n' || response[i_counter + 2] != '\r' || response[i_counter + 3] != '\n'){
    i_counter++;
  }
  return &response[i_counter + 4];
}



struct hostent *server;
struct sockaddr_in serveraddr;
int populate(api_request *request, cJSON* object){
  if(strcmp(request->method_name, "register_formula") == 0){
    populate_json_formula(object, request->formula);
  }
  else if(strcmp(request->method_name, "register_worker") == 0){
    populate_json_worker(object, request->worker);
  }
  else if(strcmp(request->method_name, "register_solver") == 0){
    populate_json_solver(object, request->solver);
  }
  else if(strcmp(request->method_name, "register_command") == 0){
    populate_json_command(object, request->command);
  }
  else if(strcmp(request->method_name, "register_trial") == 0){
    populate_json_trial(object, request->trial);
  }
  return 0;
}
int send_request(api_request *request){
  cJSON * object;
  char request_string[4096];
  SATBench_error = 0;
  object = cJSON_CreateObject();
  populate(request, object);
  cJSON * root = cJSON_CreateObject();
  cJSON_AddItemToObject(root, "request", object);
  cJSON_AddStringToObject(root, "method_name", request->method_name);
  cJSON_AddStringToObject(root,"apikey",request->apikey);
  
         int flag = 1;
  int tcpSocket = socket(AF_INET, SOCK_STREAM, 0);

  if(server == NULL){
    server = gethostbyname(API_HOST);

    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;

    bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);

    serveraddr.sin_port = htons(80);
  }
  connect(tcpSocket, (struct sockaddr *) &serveraddr, sizeof(serveraddr));
  
  char* args = cJSON_PrintUnformatted(root);
  sprintf(request_string, "GET %s?json=%s HTTP/1.1\r\nHost: %s\r\n\r\n\r\n", API_PAGE, curl_easy_escape(NULL, args, strlen(args)), API_HOST);
  send(tcpSocket, request_string, strlen(request_string), 0);
  bzero(request_string, 4096);
  int result = recv(tcpSocket, request_string, 4095, 0);
  close(tcpSocket);
  char* body = get_body(request_string);
  object = cJSON_Parse(body);
  if(cJSON_GetObjectItem(object, "error") != NULL){
    SATBench_error = cJSON_GetObjectItem(object, "code")->valueint;
    fprintf(stderr,  "%s\n",cJSON_GetObjectItem(object, "message")->valuestring);
  }
  else if(strcmp(request->method_name, "register_formula") == 0){
    populate_formula_json(object, request->formula);
  }
  else if(strcmp(request->method_name, "register_worker") == 0){
    populate_worker_json(object, request->worker);
  }
  else if(strcmp(request->method_name, "register_solver") == 0){
    populate_solver_json(object, request->solver);
  }
  else if(strcmp(request->method_name, "register_command") == 0){
    populate_command_json(object, request->command);
  }
  else if(strcmp(request->method_name, "register_trial") == 0){
    populate_trial_json(object, request->trial);
  }
  return 0;
}


int SATBench_register_formula(char* apikey, struct SATBench_formula * formula){
  api_request request;
  request.formula = formula;
  strcpy(request.method_name, "register_formula");
  strcpy(request.apikey, apikey);
  
  return send_request(&request);
}
int SATBench_register_worker(char* apikey, struct SATBench_worker * worker){
  api_request request;
  request.worker = worker;
  strcpy(request.method_name, "register_worker");
  strcpy(request.apikey, apikey);
  
  return send_request(&request);
}
int SATBench_register_solver(char* apikey, struct SATBench_solver * solver){
  api_request request;
  request.solver = solver;
  strcpy(request.method_name, "register_solver");
  strcpy(request.apikey, apikey);
  
  return send_request(&request);
}
int SATBench_register_command(char* apikey, struct SATBench_command * command){
  api_request request;
  request.command = command;
  strcpy(request.method_name, "register_command");
  strcpy(request.apikey, apikey);
  return send_request(&request);
}
int SATBench_register_trial(char* apikey, struct SATBench_trial * trial){
  api_request request;
  request.trial = trial;
  strcpy(request.method_name, "register_trial");
  strcpy(request.apikey, apikey);
  return send_request(&request);
}
int SATBench_easy(/*...TODO...*/);
