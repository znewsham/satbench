#include "trial.h"
#include "main.h"
#include <SATBench.h>
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <openssl/evp.h>
int register_trial(int argsc, char** argsv){
  int c = 0;
  int option_index = 0;
  struct SATBench_trial trial;
  bzero(&trial, sizeof(trial));
  strcpy(apikey,"");
   while (1) {
      c = getopt_long (argsc, argsv, options_string[TRIAL_INDEX],trial_options, &option_index);
      if (c == -1){
        break;
      }
      switch(c){
        case 'a':
          strcpy(apikey, optarg);
          break;
        case 'b':
          trial.command_id = atoi(optarg);
          break;
        case 'c':
          trial.formula_id = atoi(optarg);
          break;
        case 'd':
          trial.worker_id = atoi(optarg);
          break;
        case 'e':
          trial.wc_time_millis = atoi(optarg);
          break;
        case 'f':
          trial.cpu_time_millis = atoi(optarg);
          break;
        case 'g':
          trial.result = atoi(optarg);
          break;
        case 'h':
          trial.inputs = atoi(optarg);
          break;
        case 'i':
          trial.outputs = atoi(optarg);
          break;
        case 'j':
          trial.max_resident = atoi(optarg);
          break;
        case 'k':
          trial.major_pagefaults = atoi(optarg);
          break;
        case 'l':
          trial.minor_pagefaults = atoi(optarg);
          break;
        case 'm':
          trial.swaps = atoi(optarg);
          break;
        case 0:
        case '?':
          break;
        default:
          abort();
      }
   }
  if(strcmp(apikey,"") == 0){
    fprintf(stderr, "No API key defined\n");
    return -1;
  }
  if(trial.command_id == 0){
    fprintf(stderr,"No Command ID provided\n");
    return -1;
  }
  if(trial.worker_id == 0){
    fprintf(stderr,"No Worker ID provided\n");
    return -1;
  }
  if(trial.formula_id == 0){
    fprintf(stderr,"No Formula ID provided\n");
    return -1;
  }
  
  SATBench_register_trial(apikey, &trial);
  
  return trial.trial_id;
}
