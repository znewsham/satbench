#include <SATBench.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <openssl/evp.h>
#include "command.h"
#include "worker.h"
#include "formula.h"
#include "trial.h"
#include "solver.h"
#include "bulk.h"
#include "main.h"


static char **options_type[] = {
  command_types,
  worker_types,
  formula_types,
  solver_types,
  trial_types,
  bulk_types
};
void help();


static struct option *options[] = {
  command_options,
  worker_options,
  formula_options,
  solver_options,
  trial_options,
  bulk_options
};

void name_from_file(char* name, char* file){
  char* p = strtok(file, "/");
  char* p1;
  while(p != NULL){
    p1 = strtok(NULL, "/");
    if(p1 == NULL){
      strncpy(name, p, 256);
      break;
    }
    else{
      p = p1;
    }
  }
}

struct option* option_from_char(char c, struct option set[]){
  struct option *current;
  int i = 0;
  do{
    current = &set[i];
    if(current->val == c){
      return current;
    }
    i ++;
  }while(current->val != 0);
  
  return 0;
}

void register_help(int index){
  int i = 0;
  while(1){
    struct option o = options[index][i];
    if(o.name == 0){
      break;
    }
    char flag[32];
    if(o.has_arg == required_argument){
      strcpy(flag, "required");
    }
    else if(o.has_arg == no_argument){
      strcpy(flag, "flag");
    }
    else if(o.has_arg == optional_argument){
      strcpy(flag, "optional");
    }
    printf("--%-30s%-15s%-15s\n", o.name, flag, options_type[index][i]);
    i++;
  }
}
void help(){
  printf("usage:\n");
  printf("SATBench [get|register] [formula|worker|command|solver|trial|bulk] [OPTIONS]\n");
  printf("-----------------------------\n");
  printf("SATBench register formula [OPTIONS]\n");
  register_help(FORMULA_INDEX);
  printf("-----------------------------\n");
  printf("SATBench register command [OPTIONS]\n");
  register_help(COMMAND_INDEX);
  printf("-----------------------------\n");
  printf("SATBench register trial [OPTIONS]\n");
  register_help(TRIAL_INDEX);
  printf("-----------------------------\n");
  printf("SATBench register solver [OPTIONS]\n");
  register_help(SOLVER_INDEX);
  printf("-----------------------------\n");
  printf("SATBench register worker [OPTIONS]\n");
  register_help(WORKER_INDEX);
  printf("-----------------------------\n");
  printf("SATBench register bulk [OPTIONS]\nregister a set of formula/trial from a csv file");
  register_help(BULK_INDEX);
}


int realmain(int argsc, char** argsv){
  if(argsc < 3){
    help();
    return 0;
  }
  int ret = 0;
  if(strcmp(argsv[1], "register") == 0){
    if(strcmp(argsv[2], "formula") == 0){
      ret = register_formula(argsc - 2, &argsv[2]);
    }
    else if(strcmp(argsv[2], "solver") == 0){
      ret = register_solver(argsc - 2, &argsv[2]);
    }
    else if(strcmp(argsv[2], "command") == 0){
      ret = register_command(argsc - 2, &argsv[2]);
    }
    else if(strcmp(argsv[2], "worker") == 0){
      ret = register_worker(argsc - 2, &argsv[2]);
    }
    else if(strcmp(argsv[2], "trial") == 0){
      ret = register_trial(argsc - 2, &argsv[2]);
    }
    else if(strcmp(argsv[2], "bulk") == 0){
      ret = register_bulk(argsc - 2, &argsv[2]);
    }
    else{
      help();
      return 0;
    }
    printf("%d\n", ret);
    return ret;
  }
  else if(strcmp(argsv[1], "get") == 0){
    return -1;
  }
  else{
    help();
    return 0;
  }
}
