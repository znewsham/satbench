/* 
 * File:   trial.h
 * Author: zacknewsham
 *
 * Created on October 16, 2014, 2:22 AM
 */

#include <getopt.h>
#include "main.h"
#ifndef TRIAL_H
#define	TRIAL_H

#ifdef	__cplusplus
extern "C" {
#endif

static char* trial_types[] = {"string(128)\0","int\0","int\0","int\0","int\0","int\0","int\0","int\0","int\0","int\0","int\0","int\0","int\0"};

static struct option trial_options[] = 
{ //trial options
  {"apikey",  required_argument, 0, 'a'},
  {"command", required_argument, 0, 'b'},
  {"formula",required_argument, 0, 'c'},
  {"worker", required_argument, 0, 'd'},
  {"wctime", required_argument, 0, 'e'},
  {"cputime", required_argument, 0, 'f'},
  {"result", optional_argument, 0, 'g'},
  {"inputs", optional_argument, 0, 'h'},
  {"outputs", optional_argument, 0, 'i'},
  {"maxres", optional_argument, 0, 'j'},
  {"majors", optional_argument, 0, 'k'},
  {"minors", optional_argument, 0, 'l'},
  {"swaps", optional_argument, 0, 'm'},
  {0, 0, 0, 0}
};
int register_trial(int argsc, char** argsv);

#ifdef	__cplusplus
}
#endif

#endif	/* TRIAL_H */

