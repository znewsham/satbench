#include "command.h"
#include "main.h"
#include <SATBench.h>
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <openssl/evp.h>

int register_command(int argsc, char** argsv){
  int c = 0;
  int option_index = 0;
  struct SATBench_command command;
  char *keys[32];
  char *values[32];
  char *start;
  int num_args = 0;
  int i;
  int need_value = 0;
  bzero(&command, sizeof(struct SATBench_command));
  strcpy(apikey,"");
  command.threads = 1;
   while (1) {
      c = getopt_long (argsc, argsv, options_string[COMMAND_INDEX],command_options, &option_index);
      if (c == -1){
        break;
      }
      switch(c){
        case 'a':
          strcpy(apikey, optarg);
          break;
        case 'b':
          command.solver_id = atoi(optarg);
          break;
        case 'd':
          command.threads = atoi(optarg);
          break;
        case 'c':
          if(strlen(optarg) > 2048){
            fprintf(stderr, "Options too long (2048)");
            return -1;
          }
          start= optarg;
          for(i = 0; i < strlen(start); i++){
            if(start[i] == '='){
              need_value=1;
              keys[num_args] = malloc(32);
              bzero(keys[num_args],32);
              if(i > 32){
                fprintf(stderr, "%d\n", i);
                fprintf(stderr, "%s name too long (32)", start);
                return -1;
              }
              strncpy(keys[num_args], start, i);
              start = &start[i + 1];
              i=0;
            }
            if(start[i] == ','){
              need_value=0;
              values[num_args] = malloc(128);
              bzero(values[num_args],128);
              if(strlen(start) > 128){
                fprintf(stderr, "%s value too long (128)", start);
                return -1;
              }
              strncpy(values[num_args], start, i);
              start = &start[i + 1];
              i=0;
              num_args++;
            }
          }
          if(need_value){
            values[num_args] = malloc(128);
              if(strlen(start) > 128){
                fprintf(stderr, "%s value too long (128)", start);
                return -1;
              }
            strncpy(values[num_args], start, i);
            num_args++;
          }
        case 0:
          break;
        case '?':
          break;
        default:
          abort();
      }
   }
  
  command.keys = keys;
  command.values = values;
  command.length = num_args;
  if(strcmp(apikey,"") == 0){
    fprintf(stderr, "No API key defined\n");
    return -1;
  }
  if(command.solver_id == 0){
    fprintf(stderr,"No Solver ID provided\n");
    return -1;
  }
  command.simplification = simp_flag;
  command.output = noout_flag == 1 ? 0 : 1;
  SATBench_register_command(apikey, &command);
  
  return command.command_id;
}
