#include "solver.h"
#include "main.h"
#include <SATBench.h>
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <openssl/evp.h>

int register_solver(int argsc, char** argsv){
  int c = 0;
  int option_index = 0;
  FILE* file;
  int read;
  int BUFFER_LENGTH = 1024;
  char buffer[BUFFER_LENGTH];
  char filename[512];
  unsigned int length;
  strcpy(apikey,"");
  strcpy(filename,"");
  EVP_MD_CTX *mdctx;
  const EVP_MD *md;
  
  int num_args = 0;
  char keys[16][32];
  char values[16][128];
  
  struct SATBench_solver solver;
   while (1) {
      c = getopt_long (argsc, argsv, options_string[SOLVER_INDEX],solver_options, &option_index);
      if (c == -1){
        break;
      }
      switch(c){
        case 'a':
          strcpy(apikey, optarg);
          break;
        case 'b':
          strcpy(filename, optarg);
          name_from_file(solver.solver_name, filename);
          break;
        case 'c':
          strcpy(solver.solver_name, optarg);
          break;
        case 'd':
          strcpy(solver.version, optarg);
          break;
        case 0:
        case '?':
          break;
        default:
          help();
          abort();
      }
   }
  if(strcmp(filename,"") == 0){
    perror("No file provided");
    return -1;
  }
  file = fopen(filename, "r");
  if(file <= 0){
    printf("%s\n", filename);
    perror("No file");
    return -1;
  }
  if(strcmp(apikey,"") == 0){
    fprintf(stderr, "No API key defined\n");
    return -1;
  }
  if(strcmp(solver.version,"") == 0){
    fprintf(stderr, "No Version defined\n");
    return -1;
  }
  mdctx = EVP_MD_CTX_create();
  length = 0;
  OpenSSL_add_all_digests();
  md = EVP_get_digestbyname("sha256");
  EVP_DigestInit_ex(mdctx, md, NULL);
  
  while((read = fread((void*)buffer, 1, BUFFER_LENGTH, file)) != 0){
    EVP_DigestUpdate(mdctx, buffer, read);
    length += read;
    if(read < BUFFER_LENGTH){
      break;
    }
  }
  EVP_DigestFinal_ex(mdctx, solver.sha256, &length);
  EVP_MD_CTX_destroy(mdctx);
  EVP_cleanup();
  SATBench_register_solver(apikey, &solver);
  return solver.solver_id;
}
