/* 
 * File:   formula.h
 * Author: zacknewsham
 *
 * Created on October 16, 2014, 2:22 AM
 */

#include <getopt.h>
#include "main.h"
#ifndef FORMULA_H
#define	FORMULA_H

#ifdef	__cplusplus
extern "C" {
#endif
static char* formula_types[] = {
  "string(128)\0",
  "string(512)\0", 
  "string(255)\0",
  "int\0",
  "int\0",
  "string(2048)\0",
  "\0",
  "\0",
  "\0",
  "\0",
  "\0",
  "\0",
  "\0",
  "\0"};
static struct option formula_options[] = 
{ //formula options
  {"apikey",  required_argument, 0, 'a'},
  {"file",  required_argument, 0, 'b'},
  {"name",  optional_argument, 0, 'c'},
  {"clauses",  optional_argument, 0, 'd'},
  {"vars",  optional_argument, 0, 'e'},
  {"options",  optional_argument, 0, 'f'},
  
  {"upload", no_argument,       &upload_flag, 1},
  {"simp",   no_argument,       &simp_flag, 1},
  {"app",   no_argument,       &app_flag, 1},
  {"hard",   no_argument,       &hard_flag, 1},
  {"random",   no_argument,       &random_flag, 1},
  {"not-app",   no_argument,       &app_flag, 0},
  {"not-hard",   no_argument,       &hard_flag, 0},
  {"not-random",   no_argument,       &random_flag, 0},
  {0, 0, 0, 0}
};

int register_formula(int argsc, char** argsv);

#ifdef	__cplusplus
}
#endif

#endif	/* FORMULA_H */

