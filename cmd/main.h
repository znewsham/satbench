/* 
 * File:   main.h
 * Author: zacknewsham
 *
 * Created on October 16, 2014, 2:21 AM
 */

#ifndef MAIN_H
#define	MAIN_H

#ifdef	__cplusplus
extern "C" {
#endif

#define COMMAND_INDEX 0
#define WORKER_INDEX 1
#define FORMULA_INDEX 2
#define SOLVER_INDEX 3
#define TRIAL_INDEX 4
#define BULK_INDEX 5
#include <SATBench.h>
#include <getopt.h>
  
int realmain(int argsc, char** argsv);
static char* options_string[] = {
  "a:b:c:d:",
  "a:b:c:d:e:f:g:h:i:j:k:l:m:n:o:p:q:",
  "a:b:c:d:e:f:",
  "a:b:c:d:",
  "a:b:c:d:e:f:g:h:i:j:k:l:m:",
  "a:b:c:d:e:f:g:h:i:j:"
};
void help();
static int simp_flag;
static int app_flag = -1;
static int hard_flag = -1;
static int random_flag = -1;
static int noout_flag = 0;
static int upload_flag;
static char apikey[128];
void name_from_file(char* name, char* file);
struct option* option_from_char(char c, struct option set[]);


#ifdef	__cplusplus
}
#endif

#endif	/* MAIN_H */

