/*
 * File:   bulk.c
 * Author: zacknewsham
 *
 * Created on Nov 2, 2014, 2:44:16 PM
 */

#include <stdio.h>
#include <stdlib.h>
#define TEST
#include "satbench.h"
#include <SATBench.h>
#include "../main.h"
#include <CUnit/Basic.h>

int init_suite(void) {
  return 0;
}

int clean_suite(void) {
  return 0;
}

int beforeTest(){
  optind = 1;
  
  register_formula_count = 0;
  register_trial_count = 0;
  register_solver_count = 0;
  register_command_count = 0;
  register_worker_count = 0;
}

void register_bulk_no_apikey_fail() {
  beforeTest();
  char* args[] = {
    "bulk"
  };
  
  CU_ASSERT(register_bulk(1, args) == -1);
  CU_ASSERT(register_formula_count == 0);
  CU_ASSERT(register_trial_count == 0);
}

void register_bulk_no_worker_fail() {
  beforeTest();
  char* args[] = {
    "bulk",
    "--apikey=12345"
  };
  
  CU_ASSERT(register_bulk(2, args) == -1);
  CU_ASSERT(register_formula_count == 0);
  CU_ASSERT(register_trial_count == 0);
}

void register_bulk_no_command_fail() {
  beforeTest();
  char* args[] = {
    "bulk",
    "--apikey=12345",
    "--worker=1"
  };
  
  CU_ASSERT(register_bulk(3, args) == -1);
  CU_ASSERT(register_formula_count == 0);
  CU_ASSERT(register_trial_count == 0);
}

void register_bulk_no_filefield_fail() {
  beforeTest();
  char* args[] = {
    "bulk",
    "--apikey=12345",
    "--worker=1",
    "--command=1"
  };
  
  CU_ASSERT(register_bulk(4, args) == -1);
  CU_ASSERT(register_formula_count == 0);
  CU_ASSERT(register_trial_count == 0);
}

void register_bulk_no_file_fail() {
  beforeTest();
  char* args[] = {
    "bulk",
    "--apikey=12345",
    "--worker=1",
    "--command=1",
    "--filefield=file"
  };
  
  CU_ASSERT(register_bulk(5, args) == -1);
  CU_ASSERT(register_formula_count == 0);
  CU_ASSERT(register_trial_count == 0);
}

void register_bulk_bad_file_fail() {
  beforeTest();
  char* args[] = {
    "bulk",
    "--apikey=12345",
    "--worker=1",
    "--command=1",
    "--filefield=file",
    "--file=/test"
  };
  
  CU_ASSERT(register_bulk(6, args) == -1);
  CU_ASSERT(register_formula_count == 0);
  CU_ASSERT(register_trial_count == 0);
}


void register_bulk_all_bad_rows() {
  beforeTest();
  char* args[] = {
    "bulk",
    "--apikey=12345",
    "--worker=1",
    "--command=1",
    "--filefield=ff",
    "--file=./testFiles/test2.csv",
    "--cpufield=tf"
  };
  
  CU_ASSERT(register_bulk(7, args) == 1);
  CU_ASSERT(register_formula_count == 0);
  CU_ASSERT(register_trial_count == 0);
}


void register_bulk_one_bad_rows() {
  beforeTest();
  char* args[] = {
    "bulk",
    "--apikey=12345",
    "--worker=1",
    "--command=1",
    "--filefield=ff",
    "--file=./testFiles/test3.csv",
    "--cpufield=tf"
  };
  
  formula_input = (struct SATBench_formula*)malloc(sizeof(struct SATBench_formula));
  trial_input = (struct SATBench_trial*)malloc(sizeof(struct SATBench_trial));
  
  CU_ASSERT(register_bulk(7, args) == 1);
  CU_ASSERT(register_formula_count == 1);
  CU_ASSERT(register_trial_count == 1);
}

void register_bulk_sucess() {
  beforeTest();
  char* args[] = {
    "bulk",
    "--apikey=12345",
    "--worker=1",
    "--command=1",
    "--filefield=ff",
    "--file=./testFiles/test1.csv",
    "--cpufield=tf"
  };
  
  CU_ASSERT(register_bulk(7, args) == 1);
  CU_ASSERT(register_formula_count == 1);
  CU_ASSERT(register_trial_count == 1);
}
int main() {
  CU_pSuite pSuite = NULL;

  /* Initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* Add a suite to the registry */
  pSuite = CU_add_suite("bulk", init_suite, clean_suite);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Add the tests to the suite */
  if (
          (NULL == CU_add_test(pSuite, "register_bulk_no_apikey_fail", register_bulk_no_apikey_fail)) ||
          (NULL == CU_add_test(pSuite, "register_bulk_no_worker_fail", register_bulk_no_worker_fail)) ||
          (NULL == CU_add_test(pSuite, "register_bulk_no_command_fail", register_bulk_no_command_fail)) ||
          (NULL == CU_add_test(pSuite, "register_bulk_no_filefield_fail", register_bulk_no_filefield_fail)) ||
          (NULL == CU_add_test(pSuite, "register_bulk_no_file_fail", register_bulk_no_file_fail)) ||
          (NULL == CU_add_test(pSuite, "register_bulk_bad_file_fail", register_bulk_bad_file_fail)) ||
          (NULL == CU_add_test(pSuite, "register_bulk_all_bad_rows", register_bulk_all_bad_rows)) ||
          (NULL == CU_add_test(pSuite, "register_bulk_one_bad_rows", register_bulk_one_bad_rows)) ||
          (NULL == CU_add_test(pSuite, "register_bulk_sucess", register_bulk_sucess))
      ) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Run all tests using the CUnit Basic interface */
  CU_basic_set_mode(CU_BRM_VERBOSE);
  CU_basic_run_tests();
  CU_cleanup_registry();
  return CU_get_error();
}
