#include <string.h>

#include "satbench.h"
int SATBench_register_formula(char* apikey, struct SATBench_formula * instance, char* file_name){
  memcpy(formula_input, instance, sizeof(struct SATBench_formula));
  memcpy(instance, &formula_return, sizeof(struct SATBench_formula));
  register_formula_count++;
  return register_formula_return;
}

int SATBench_register_worker(char* apikey, struct SATBench_worker * worker){
  memcpy(worker_input, worker, sizeof(struct SATBench_worker));
  memcpy(worker, &worker_return, sizeof(struct SATBench_worker));
  register_worker_count++;
  return register_worker_return;
}

int SATBench_register_solver(char* apikey, struct SATBench_solver * solver){
  memcpy(solver_input, solver, sizeof(struct SATBench_solver));
  memcpy(solver, &solver_return, sizeof(struct SATBench_solver));
  register_solver_count++;
  return register_solver_return;
}

int SATBench_register_command(char* apikey, struct SATBench_command * command){
  memcpy(command_input, command, sizeof(struct SATBench_command));
  memcpy(command, &command_return, sizeof(struct SATBench_command));
  register_command_count++;
  return register_command_return;
}

int SATBench_register_trial(char* apikey, struct  SATBench_trial * trial){
  memcpy(trial_input, trial, sizeof(struct SATBench_trial));
  memcpy(trial, &trial_return, sizeof(struct SATBench_trial));
  register_trial_count++;
  return register_trial_return;
}