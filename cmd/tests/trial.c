/*
 * File:   trial.c
 * Author: zacknewsham
 *
 * Created on Nov 2, 2014, 2:44:16 PM
 */

#include <stdio.h>
#include <stdlib.h>
#define TEST
#include "satbench.h"
#include <SATBench.h>
#include "../main.h"
#include <CUnit/Basic.h>

int init_suite(void) {
  return 0;
}

int clean_suite(void) {
  return 0;
}

int beforeTest(){
  optind = 1;
  
  register_formula_count = 0;
  register_trial_count = 0;
  register_solver_count = 0;
  register_command_count = 0;
  register_worker_count = 0;
}
void register_trial_no_worker_fail() {
  beforeTest();
  char* args[] = {
    "trial",
    "--command=1",
    "--formula=2"
  };
  trial_input = (struct SATBench_trial*)malloc(sizeof(struct SATBench_trial));
  trial_return.trial_id=75;
  
  CU_ASSERT(register_trial(3, args) == -1);
  CU_ASSERT(register_trial_count == 0);
}

void register_trial_no_command_fail() {
  beforeTest();
  char* args[] = {
    "trial",
    "--worker=3",
    "--formula=4"
  };
  trial_input = (struct SATBench_trial*)malloc(sizeof(struct SATBench_trial));
  trial_return.trial_id=75;
  
  CU_ASSERT(register_trial(3, args) == -1);
  CU_ASSERT(register_trial_count == 0);
}

void register_trial_no_formula_fail() {
  beforeTest();
  char* args[] = {
    "trial",
    "--command=5",
    "--worker=6"
  };
  trial_input = (struct SATBench_trial*)malloc(sizeof(struct SATBench_trial));
  trial_return.trial_id=75;
  
  CU_ASSERT(register_trial(3, args) == -1);
  CU_ASSERT(register_trial_count == 0);
}

void register_trial_with_time() {
  beforeTest();
  char* args[] = {
    "trial",
    "--apikey=12345",
    "--command=7",
    "--worker=8",
    "--formula=9",
    "--wctime=100000",
    "--cputime=80000"
  };
  trial_input = (struct SATBench_trial*)malloc(sizeof(struct SATBench_trial));
  trial_return.trial_id=75;
  
  CU_ASSERT(register_trial(7, args) == 75);
  CU_ASSERT(trial_input->command_id == 7);
  CU_ASSERT(trial_input->worker_id == 8);
  CU_ASSERT(trial_input->formula_id == 9);
  CU_ASSERT(trial_input->wc_time_millis == 100000);
  CU_ASSERT(trial_input->cpu_time_millis == 80000);
  CU_ASSERT(register_trial_count == 1);
}

void register_trial_noapikey_fail() {
  beforeTest();
  char* args[] = {
    "trial",
  };
  trial_input = (struct SATBench_trial*)malloc(sizeof(struct SATBench_trial));
  trial_return.trial_id=75;
  
  CU_ASSERT(register_trial(2, args) == -1);
  CU_ASSERT(register_trial_count == 0);
}
int main() {
  CU_pSuite pSuite = NULL;

  /* Initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* Add a suite to the registry */
  pSuite = CU_add_suite("trial", init_suite, clean_suite);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Add the tests to the suite */
  if (
          (NULL == CU_add_test(pSuite, "register_trial_no_worker_fail", register_trial_no_worker_fail)) ||
          (NULL == CU_add_test(pSuite, "register_trial_no_formula_fail", register_trial_no_formula_fail)) ||
          (NULL == CU_add_test(pSuite, "register_trial_no_command_fail", register_trial_no_command_fail)) ||
          (NULL == CU_add_test(pSuite, "register_trial_with_time", register_trial_with_time)) ||
          (NULL == CU_add_test(pSuite, "register_trial_noapikey_fail", register_trial_noapikey_fail))
      ) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Run all tests using the CUnit Basic interface */
  CU_basic_set_mode(CU_BRM_VERBOSE);
  CU_basic_run_tests();
  CU_cleanup_registry();
  return CU_get_error();
}
