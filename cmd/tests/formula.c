/*
 * File:   formula.c
 * Author: zacknewsham
 *
 * Created on Nov 2, 2014, 2:44:16 PM
 */

#include <stdio.h>
#include <stdlib.h>
#define TEST
#include "satbench.h"
#include <SATBench.h>
#include "../main.h"
#include <CUnit/Basic.h>

int init_suite(void) {
  return 0;
}

int clean_suite(void) {
  return 0;
}

int beforeTest(){
  optind = 1;
  
  register_formula_count = 0;
  register_trial_count = 0;
  register_solver_count = 0;
  register_command_count = 0;
  register_worker_count = 0;
}

void register_formula_no_details_no_name() {
  beforeTest();
  char* args[] = {
    "formula",
    "--apikey=12345",
    "--file=/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf",
  };
  formula_input = (struct SATBench_formula*)malloc(sizeof(struct SATBench_formula));
  formula_return.formula_id=75;
  
  CU_ASSERT(register_formula(3, args) == 75);
  CU_ASSERT(strcmp(formula_input->formula_name,"test1.cnf") == 0);
  CU_ASSERT(register_formula_count == 1);
}

void register_formula_no_details_name() {
  beforeTest();
  char* args[] = {
    "formula",
    "--apikey=12345",
    "--file=/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf",
    "--name=chocolate",
  };
  formula_input = (struct SATBench_formula*)malloc(sizeof(struct SATBench_formula));
  formula_return.formula_id=75;
  
  CU_ASSERT(register_formula(4, args) == 75);
  CU_ASSERT(strcmp(formula_input->formula_name,"chocolate") == 0);
  CU_ASSERT(register_formula_count == 1);
}

void register_formula_clauses_vars() {
  beforeTest();
  char* args[] = {
    "formula",
    "--apikey=12345",
    "--file=/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf",
    "--clauses=111",
    "--vars=222",
  };
  formula_input = (struct SATBench_formula*)malloc(sizeof(struct SATBench_formula));
  formula_return.formula_id=75;
  
  CU_ASSERT(register_formula(5, args) == 75);
  CU_ASSERT(formula_input->length == 2);
  CU_ASSERT(strcmp(formula_input->keys[0], "vars"));
  CU_ASSERT(strcmp(formula_input->keys[1], "clauses"));
  CU_ASSERT(strcmp(formula_input->values[1], "111"));
  CU_ASSERT(strcmp(formula_input->values[0], "222"));
  CU_ASSERT(register_formula_count == 1);
}


void register_formula_options() {
  beforeTest();
  char* args[] = {
    "formula",
    "--apikey=12345",
    "--file=/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf",
    "--options=ol_q=0.99,ol_coms=40",
  };
  formula_input = (struct SATBench_formula*)malloc(sizeof(struct SATBench_formula));
  formula_return.formula_id=75;
  
  CU_ASSERT(register_formula(5, args) == 75);
  CU_ASSERT(formula_input->length == 2);
  CU_ASSERT(strcmp(formula_input->keys[1], "ol_q"));
  CU_ASSERT(strcmp(formula_input->keys[0], "ol_coms"));
  CU_ASSERT(strcmp(formula_input->values[1], "0.99"));
  CU_ASSERT(strcmp(formula_input->values[0], "40"));
  CU_ASSERT(register_formula_count == 1);
}

void register_formula_nofile_fail() {
  beforeTest();
  char* args[] = {
    "formula",
    "--apikey=222"
  };
  formula_input = (struct SATBench_formula*)malloc(sizeof(struct SATBench_formula));
  formula_return.formula_id=75;
  
  CU_ASSERT(register_formula(2, args) == -1);
  CU_ASSERT(register_formula_count == 0);
}

void register_formula_noapikey_fail() {
  beforeTest();
  char* args[] = {
    "formula",
    "--file=/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf"
  };
  formula_input = (struct SATBench_formula*)malloc(sizeof(struct SATBench_formula));
  formula_return.formula_id=75;
  
  CU_ASSERT(register_formula(2, args) == -1);
  CU_ASSERT(register_formula_count == 0);
}

int main() {
  CU_pSuite pSuite = NULL;

  /* Initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* Add a suite to the registry */
  pSuite = CU_add_suite("formula", init_suite, clean_suite);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Add the tests to the suite */
  if (
          (NULL == CU_add_test(pSuite, "register_formula_no_details_no_name", register_formula_no_details_no_name)) ||
          (NULL == CU_add_test(pSuite, "register_formula_no_details_name", register_formula_no_details_name)) ||
          (NULL == CU_add_test(pSuite, "register_formula_clauses_vars", register_formula_clauses_vars)) ||
          (NULL == CU_add_test(pSuite, "register_formula_options", register_formula_options)) ||
          (NULL == CU_add_test(pSuite, "register_formula_nofile_fail", register_formula_nofile_fail)) ||
          (NULL == CU_add_test(pSuite, "register_formula_noapikey_fail", register_formula_noapikey_fail))
      ) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Run all tests using the CUnit Basic interface */
  CU_basic_set_mode(CU_BRM_VERBOSE);
  CU_basic_run_tests();
  CU_cleanup_registry();
  return CU_get_error();
}
