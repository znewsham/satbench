/*
 * File:   worker.c
 * Author: zacknewsham
 *
 * Created on Nov 2, 2014, 2:44:16 PM
 */

#include <stdio.h>
#include <stdlib.h>
#define TEST
#include "satbench.h"
#include <SATBench.h>
#include "../main.h"
#include <CUnit/Basic.h>

int init_suite(void) {
  return 0;
}

int clean_suite(void) {
  return 0;
}

int beforeTest(){
  optind = 1;
  
  register_formula_count = 0;
  register_trial_count = 0;
  register_solver_count = 0;
  register_command_count = 0;
  register_worker_count = 0;
}

void register_worker_no_apikey_fail() {
  beforeTest();
  char* args[] = {
    "worker"
  };
  
  worker_input = (struct SATBench_worker*)malloc(sizeof(struct SATBench_worker));
  worker_return.worker_id=75;
  CU_ASSERT(register_worker(1, args) == -1);
  CU_ASSERT(register_worker_count == 0);
}
void register_worker_no_name_fail() {
  beforeTest();
  char* args[] = {
    "worker",
    "--apikey=12345"
  };
  
  worker_input = (struct SATBench_worker*)malloc(sizeof(struct SATBench_worker));
  worker_return.worker_id=75;
  CU_ASSERT(register_worker(2, args) == -1);
  CU_ASSERT(register_worker_count == 0);
}

void register_worker_no_cpuspeed_fail() {
  beforeTest();
  char* args[] = {
    "worker",
    "--apikey=12345",
    "--name=test"
  };
  
  worker_input = (struct SATBench_worker*)malloc(sizeof(struct SATBench_worker));
  worker_return.worker_id=75;
  CU_ASSERT(register_worker(3, args) == -1);
  CU_ASSERT(register_worker_count == 0);
}

void register_worker_no_cpuarch_fail() {
  beforeTest();
  char* args[] = {
    "worker",
    "--apikey=12345",
    "--name=test",
    "--cpu-speed=100"
  };
  
  worker_input = (struct SATBench_worker*)malloc(sizeof(struct SATBench_worker));
  worker_return.worker_id=75;
  CU_ASSERT(register_worker(4, args) == -1);
  CU_ASSERT(register_worker_count == 0);
}

void register_worker_no_cpumodel_fail() {
  beforeTest();
  char* args[] = {
    "worker",
    "--apikey=12345",
    "--name=test",
    "--cpu-speed=100",
    "--cpu-arch=x86"
  };
  
  worker_input = (struct SATBench_worker*)malloc(sizeof(struct SATBench_worker));
  worker_return.worker_id=75;
  CU_ASSERT(register_worker(5, args) == -1);
  CU_ASSERT(register_worker_count == 0);
}

void register_worker_no_cpumanufacturer_fail() {
  beforeTest();
  char* args[] = {
    "worker",
    "--apikey=12345",
    "--name=test",
    "--cpu-speed=100",
    "--cpu-arch=x86",
    "--cpu-model=pentium"
  };
  
  worker_input = (struct SATBench_worker*)malloc(sizeof(struct SATBench_worker));
  worker_return.worker_id=75;
  CU_ASSERT(register_worker(6, args) == -1);
  CU_ASSERT(register_worker_count == 0);
}

void register_worker_no_cpucores_fail() {
  beforeTest();
  char* args[] = {
    "worker",
    "--apikey=12345",
    "--name=test",
    "--cpu-speed=100",
    "--cpu-arch=x86",
    "--cpu-model=pentium",
    "--cpu-manufacturer=intel"
  };
  
  worker_input = (struct SATBench_worker*)malloc(sizeof(struct SATBench_worker));
  worker_return.worker_id=75;
  CU_ASSERT(register_worker(7, args) == -1);
  CU_ASSERT(register_worker_count == 0);
}

void register_worker_no_ramsize_fail() {
  beforeTest();
  char* args[] = {
    "worker",
    "--apikey=12345",
    "--name=test",
    "--cpu-speed=100",
    "--cpu-arch=x86",
    "--cpu-model=pentium",
    "--cpu-manufacturer=intel",
    "--cpu-cores=3"
  };
  
  worker_input = (struct SATBench_worker*)malloc(sizeof(struct SATBench_worker));
  worker_return.worker_id=75;
  CU_ASSERT(register_worker(8, args) == -1);
  CU_ASSERT(register_worker_count == 0);
}

void register_worker_sucess() {
  beforeTest();
  char* args[] = {
    "worker",
    "--apikey=12345",
    "--name=test",
    "--cpu-speed=100",
    "--cpu-arch=x86",
    "--cpu-model=pentium",
    "--cpu-manufacturer=intel",
    "--cpu-cores=3",
    "--ram-kbytes=100"
  };
  
  worker_input = (struct SATBench_worker*)malloc(sizeof(struct SATBench_worker));
  worker_return.worker_id=75;
  CU_ASSERT(register_worker(9, args) == 75);
  CU_ASSERT(register_worker_count == 1);
}



int main() {
  CU_pSuite pSuite = NULL;

  /* Initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* Add a suite to the registry */
  pSuite = CU_add_suite("worker", init_suite, clean_suite);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Add the tests to the suite */
  if (
          (NULL == CU_add_test(pSuite, "register_worker_no_apikey_fail", register_worker_no_apikey_fail)) ||
          (NULL == CU_add_test(pSuite, "register_worker_no_name_fail", register_worker_no_name_fail)) ||
          (NULL == CU_add_test(pSuite, "register_worker_no_cpuspeed_fail", register_worker_no_cpuspeed_fail)) ||
          (NULL == CU_add_test(pSuite, "register_worker_no_cpuarch_fail", register_worker_no_cpuarch_fail)) ||
          (NULL == CU_add_test(pSuite, "register_worker_no_cpumodel_fail", register_worker_no_cpumodel_fail)) ||
          (NULL == CU_add_test(pSuite, "register_worker_no_cpumanufacturer_fail", register_worker_no_cpumanufacturer_fail)) ||
          (NULL == CU_add_test(pSuite, "register_worker_no_cpucores_fail", register_worker_no_cpucores_fail)) ||
          (NULL == CU_add_test(pSuite, "register_worker_no_ramsize_fail", register_worker_no_ramsize_fail)) ||
          (NULL == CU_add_test(pSuite, "register_worker_sucess", register_worker_sucess))
      ) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Run all tests using the CUnit Basic interface */
  CU_basic_set_mode(CU_BRM_VERBOSE);
  CU_basic_run_tests();
  CU_cleanup_registry();
  return CU_get_error();
}
