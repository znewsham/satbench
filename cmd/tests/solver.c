/*
 * File:   solver.c
 * Author: zacknewsham
 *
 * Created on Nov 2, 2014, 2:44:16 PM
 */

#include <stdio.h>
#include <stdlib.h>
#define TEST
#include "satbench.h"
#include <SATBench.h>
#include "../main.h"
#include <CUnit/Basic.h>

int init_suite(void) {
  return 0;
}

int clean_suite(void) {
  return 0;
}

int beforeTest(){
  optind = 1;
  
  register_formula_count = 0;
  register_trial_count = 0;
  register_solver_count = 0;
  register_command_count = 0;
  register_worker_count = 0;
}

void register_solver_no_apikey_fail() {
  beforeTest();
  char* args[] = {
    "solver",
    "--file=/usr/bin/minisat"
  };
  
  solver_input = (struct SATBench_solver*)malloc(sizeof(struct SATBench_solver));
  solver_return.solver_id=75;
  CU_ASSERT(register_solver(2, args) == -1);
  CU_ASSERT(register_solver_count == 0);
}

void register_solver_no_file_fail() {
  beforeTest();
  char* args[] = {
    "solver",
    "--apikey=12345"
  };
  
  solver_input = (struct SATBench_solver*)malloc(sizeof(struct SATBench_solver));
  solver_return.solver_id=75;
  CU_ASSERT(register_solver(2, args) == -1);
  CU_ASSERT(register_solver_count == 0);
}

void register_solver_no_version_fail() {
  beforeTest();
  char* args[] = {
    "solver",
    "--apikey=12345",
    "--file=/usr/bin/minisat"
  };
  
  solver_input = (struct SATBench_solver*)malloc(sizeof(struct SATBench_solver));
  solver_return.solver_id=75;
  CU_ASSERT(register_solver(3, args) == -1);
  CU_ASSERT(register_solver_count == 0);
}

void register_solver_no_name() {
  beforeTest();
  char* args[] = {
    "solver",
    "--apikey=12345",
    "--version=3",
    "--file=/usr/bin/minisat"
  };
  
  solver_input = (struct SATBench_solver*)malloc(sizeof(struct SATBench_solver));
  solver_return.solver_id=75;
  CU_ASSERT(register_solver(4, args) == 75);
  CU_ASSERT(strcmp(solver_input->solver_name,"minisat") == 0);
  CU_ASSERT(strcmp(solver_input->version,"3") == 0);
  CU_ASSERT(register_solver_count == 1);
}

void register_solver_name() {
  beforeTest();
  char* args[] = {
    "solver",
    "--apikey=12345",
    "--file=/usr/bin/minisat",
    "--version=3",
    "--name=choc"
  };
  
  solver_input = (struct SATBench_solver*)malloc(sizeof(struct SATBench_solver));
  solver_return.solver_id=75;
  CU_ASSERT(register_solver(5, args) == 75);
  CU_ASSERT(strcmp(solver_input->solver_name,"choc") == 0);
  CU_ASSERT(strcmp(solver_input->version,"3") == 0);
  CU_ASSERT(register_solver_count == 1);
}



int main() {
  CU_pSuite pSuite = NULL;

  /* Initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* Add a suite to the registry */
  pSuite = CU_add_suite("solver", init_suite, clean_suite);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Add the tests to the suite */
  if (
          (NULL == CU_add_test(pSuite, "register_solver_no_apikey_fail", register_solver_no_apikey_fail)) ||
          (NULL == CU_add_test(pSuite, "register_solver_no_file_fail", register_solver_no_file_fail)) ||
          (NULL == CU_add_test(pSuite, "register_solver_no_name", register_solver_no_name)) ||
          (NULL == CU_add_test(pSuite, "register_solver_name", register_solver_name)) ||
          (NULL == CU_add_test(pSuite, "register_solver_no_version_fail", register_solver_no_version_fail))
      ) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Run all tests using the CUnit Basic interface */
  CU_basic_set_mode(CU_BRM_VERBOSE);
  CU_basic_run_tests();
  CU_cleanup_registry();
  return CU_get_error();
}
