/* 
 * File:   worker_types.c
 * Author: zacknewsham
 *
 * Created on November 4, 2014, 4:03 PM
 */


#include <stdio.h>
#include <stdlib.h>
#define TEST
#include "satbench.h"
#include <SATBench.h>
#include "../main.h"
#include <CUnit/Basic.h>

int init_suite(void) {
  return 0;
}

int clean_suite(void) {
  return 0;
}

int beforeTest(){
  optind = 1;
  
  register_formula_count = 0;
  register_trial_count = 0;
  register_solver_count = 0;
  register_command_count = 0;
  register_worker_count = 0;
}


void register_worker_nametoolong_fail() {
  beforeTest();
  char* args[] = {
    "worker",
    "--apikey=12345",
    "--name=nametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolong",
    "--cpu-speed=100",
    "--cpu-arch=x86",
    "--cpu-model=pentium",
    "--cpu-manufacturer=intel",
    "--cpu-cores=3",
    "--ram-kbytes=100"
  };
  
  worker_input = (struct SATBench_worker*)malloc(sizeof(struct SATBench_worker));
  worker_return.worker_id=75;
  CU_ASSERT(register_worker(9, args) == -1);
  CU_ASSERT(register_worker_count == 0);
}

void register_worker_archtoolong_fail() {
  beforeTest();
  char* args[] = {
    "worker",
    "--apikey=12345",
    "--name=test",
    "--cpu-speed=100",
    "--cpu-arch=archtoolongarchtoolong",
    "--cpu-model=pentium",
    "--cpu-manufacturer=intel",
    "--cpu-cores=3",
    "--ram-kbytes=100"
  };
  
  worker_input = (struct SATBench_worker*)malloc(sizeof(struct SATBench_worker));
  worker_return.worker_id=75;
  CU_ASSERT(register_worker(9, args) == -1);
  CU_ASSERT(register_worker_count == 0);
}

void register_worker_modeltoolong_fail() {
  beforeTest();
  char* args[] = {
    "worker",
    "--apikey=12345",
    "--name=test",
    "--cpu-speed=100",
    "--cpu-arch=x86",
    "--cpu-model=nametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolong",
    "--cpu-manufacturer=intel",
    "--cpu-cores=3",
    "--ram-kbytes=100"
  };
  
  worker_input = (struct SATBench_worker*)malloc(sizeof(struct SATBench_worker));
  worker_return.worker_id=75;
  CU_ASSERT(register_worker(9, args) == -1);
  CU_ASSERT(register_worker_count == 0);
}

void register_worker_manufacturertoolong_fail() {
  beforeTest();
  char* args[] = {
    "worker",
    "--apikey=12345",
    "--name=test",
    "--cpu-speed=100",
    "--cpu-arch=x86",
    "--cpu-model=pentium",
    "--cpu-manufacturer=nametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolongnametoolong",
    "--cpu-cores=3",
    "--ram-kbytes=100"
  };
  
  worker_input = (struct SATBench_worker*)malloc(sizeof(struct SATBench_worker));
  worker_return.worker_id=75;
  CU_ASSERT(register_worker(9, args) == -1);
  CU_ASSERT(register_worker_count == 0);
}

int main() {
  CU_pSuite pSuite = NULL;

  /* Initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* Add a suite to the registry */
  pSuite = CU_add_suite("worker_types", init_suite, clean_suite);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Add the tests to the suite */
  if (
          (NULL == CU_add_test(pSuite, "register_worker_nametoolong_fail", register_worker_nametoolong_fail)) ||
          (NULL == CU_add_test(pSuite, "register_worker_archtoolong_fail", register_worker_archtoolong_fail)) ||
          (NULL == CU_add_test(pSuite, "register_worker_modeltoolong_fail", register_worker_modeltoolong_fail)) ||
          (NULL == CU_add_test(pSuite, "register_worker_manufacturertoolong_fail", register_worker_manufacturertoolong_fail))
      ) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Run all tests using the CUnit Basic interface */
  CU_basic_set_mode(CU_BRM_VERBOSE);
  CU_basic_run_tests();
  CU_cleanup_registry();
  return CU_get_error();
}
