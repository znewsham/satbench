/* 
 * File:   satlib.h
 * Author: zacknewsham
 *
 * Created on November 2, 2014, 3:04 PM
 */

#include <SATBench.h>
#ifndef SATLIB_H
#define	SATLIB_H

#ifdef	__cplusplus
extern "C" {
#endif

int register_formula_count;
int register_trial_count;
int register_solver_count;
int register_command_count;
int register_worker_count;

int register_formula_return;
int register_worker_return;
int register_solver_return;
int register_command_return;
int register_trial_return;

struct SATBench_formula  formula_return;
struct SATBench_worker  worker_return;
struct SATBench_solver  solver_return;
struct SATBench_command  command_return;
struct SATBench_trial  trial_return;
  
struct SATBench_formula * formula_input;
struct SATBench_worker * worker_input;
struct SATBench_solver * solver_input;
struct SATBench_command * command_input;
struct SATBench_trial * trial_input;

int MockSATBench_register_formula(char* apikey, struct SATBench_formula * instance, char* file_name);
int MockSATBench_register_worker(char* apikey, struct SATBench_worker * worker);
int MockSATBench_register_solver(char* apikey, struct SATBench_solver * solver);
int MockSATBench_register_command(char* apikey, struct SATBench_command * command);
int MockSATBench_register_trial(char* apikey, struct  SATBench_trial * trial);



#ifdef	__cplusplus
}
#endif

#endif	/* SATLIB_H */

