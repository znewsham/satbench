/* 
 * File:   worker_types.c
 * Author: zacknewsham
 *
 * Created on November 4, 2014, 4:03 PM
 */


#include <stdio.h>
#include <stdlib.h>
#define TEST
#include "satbench.h"
#include <SATBench.h>
#include "../main.h"
#include <CUnit/Basic.h>

int init_suite(void) {
  return 0;
}

int clean_suite(void) {
  return 0;
}

int beforeTest(){
  optind = 1;
  
  register_formula_count = 0;
  register_trial_count = 0;
  register_solver_count = 0;
  register_command_count = 0;
  register_worker_count = 0;
}


void register_formula_filetoolong_fail() {
  beforeTest();
  char* args[] = {
    "formula",
    "--apikey=12345",
    "--file=/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf",
    "--clauses=111",
    "--vars=222",
  };
  
  CU_ASSERT(register_formula(5, args) == -1);
  CU_ASSERT(register_formula_count == 0);
}

void register_formula_nametoolong_fail() {
  beforeTest();
  char* args[] = {
    "formula",
    "--apikey=12345",
    "--file=/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf",
    "--name=/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf",
    "--vars=222",
  };
  
  CU_ASSERT(register_formula(5, args) == -1);
  CU_ASSERT(register_formula_count == 0);
}

void register_formula_clausestoolong_fail() {
  beforeTest();
  char* args[] = {
    "formula",
    "--apikey=12345",
    "--file=/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf",
    "--clauses=/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf",
    "--vars=222",
  };
  
  CU_ASSERT(register_formula(5, args) == -1);
  CU_ASSERT(register_formula_count == 0);
}

void register_formula_optionstoolong_fail() {
  beforeTest();
  char* args[] = {
    "formula",
    "--apikey=12345",
    "--file=/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf",
    "--options=/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf"
  };
  
  CU_ASSERT(register_formula(4, args) == -1);
  CU_ASSERT(register_formula_count == 0);
}

void register_formula_optiontoolong_fail() {
  beforeTest();
  char* args[] = {
    "formula",
    "--apikey=12345",
    "--file=/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf",
    "--options=111test=/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf/home/zacknewsham/Sites/SATBench/cmd/testCNF/test1.cnf",
  };
  
  CU_ASSERT(register_formula(4, args) == -1);
  CU_ASSERT(register_formula_count == 0);
}

int main() {
  CU_pSuite pSuite = NULL;

  /* Initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* Add a suite to the registry */
  pSuite = CU_add_suite("formula_types", init_suite, clean_suite);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Add the tests to the suite */
  if (
          (NULL == CU_add_test(pSuite, "register_formula_filetoolong_fail", register_formula_filetoolong_fail)) ||
          (NULL == CU_add_test(pSuite, "register_formula_nametoolong_fail", register_formula_nametoolong_fail)) ||
          (NULL == CU_add_test(pSuite, "register_formula_optionstoolong_fail", register_formula_optionstoolong_fail)) ||
          (NULL == CU_add_test(pSuite, "register_formula_optiontoolong_fail", register_formula_optiontoolong_fail)) ||
          (NULL == CU_add_test(pSuite, "register_formula_clausestoolong_fail", register_formula_clausestoolong_fail))
      ) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Run all tests using the CUnit Basic interface */
  CU_basic_set_mode(CU_BRM_VERBOSE);
  CU_basic_run_tests();
  CU_cleanup_registry();
  return CU_get_error();
}
