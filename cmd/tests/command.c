/*
 * File:   command.c
 * Author: zacknewsham
 *
 * Created on Nov 2, 2014, 2:44:16 PM
 */

#include <stdio.h>
#include <stdlib.h>
#define TEST
#include "satbench.h"
#include <SATBench.h>
#include "../main.h"
#include <CUnit/Basic.h>

int init_suite(void) {
  return 0;
}

int clean_suite(void) {
  return 0;
}

int beforeTest(){
  optind = 1;
  
  register_formula_count = 0;
  register_trial_count = 0;
  register_solver_count = 0;
  register_command_count = 0;
  register_worker_count = 0;
}

void register_command_no_solver_fail() {
  beforeTest();
  char* args[] = {
    "command",
    "--apikey=12345"
  };
  command_input = (struct SATBench_command*)malloc(sizeof(struct SATBench_command));
  command_return.command_id=75;
  
  CU_ASSERT(register_command(2, args) == -1);
  CU_ASSERT(register_command_count == 0);
}

void register_command_no_apikey_fail() {
  beforeTest();
  char* args[] = {
    "command",
    "--solver=1"
  };
  command_input = (struct SATBench_command*)malloc(sizeof(struct SATBench_command));
  command_return.command_id=75;
  CU_ASSERT(register_command(2, args) == -1);
  CU_ASSERT(register_command_count == 0);
}

void register_command_solver_args() {
  beforeTest();
  char* args[] = {
    "command",
    "--solver=1",
    "--apikey=12345",
    "--args=rabble",
    "--threads=3"
  };
  command_input = (struct SATBench_command*)malloc(sizeof(struct SATBench_command));
  command_return.command_id=75;
  CU_ASSERT(register_command(5, args) == 75);
  CU_ASSERT(command_input->solver_id == 1);
  CU_ASSERT(command_input->threads == 3);
  CU_ASSERT(command_input->simplification == 0);
  CU_ASSERT(command_input->output == 1);
  CU_ASSERT(strcmp(command_input->args,"rabble") == 0);
  CU_ASSERT(register_command_count == 1);
}

void register_command_solver_args_flags() {
  beforeTest();
  char* args[] = {
    "command",
    "--solver=1",
    "--apikey=12345",
    "--args=rabble",
    "--threads=3",
    "--simp",
    "--noout"
  };
  command_input = (struct SATBench_command*)malloc(sizeof(struct SATBench_command));
  command_return.command_id=75;
  CU_ASSERT(register_command(7, args) == 75);
  CU_ASSERT(command_input->solver_id == 1);
  CU_ASSERT(command_input->threads == 3);
  CU_ASSERT(command_input->simplification == 1);
  CU_ASSERT(command_input->output == 0);
  CU_ASSERT(strcmp(command_input->args,"rabble") == 0);
  CU_ASSERT(register_command_count == 1);
}

void register_command_solver_args_nothreads() {
  beforeTest();
  char* args[] = {
    "command",
    "--solver=1",
    "--apikey=12345"
  };
  command_input = (struct SATBench_command*)malloc(sizeof(struct SATBench_command));
  command_return.command_id=75;
  CU_ASSERT(register_command(3, args) == 75);
  CU_ASSERT(command_input->solver_id == 1);
  CU_ASSERT(command_input->threads == 1);
  CU_ASSERT(register_command_count == 1);
}


int main() {
  CU_pSuite pSuite = NULL;

  /* Initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* Add a suite to the registry */
  pSuite = CU_add_suite("command", init_suite, clean_suite);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Add the tests to the suite */
  if (
          (NULL == CU_add_test(pSuite, "register_command_no_solver_fail", register_command_no_solver_fail)) ||
          (NULL == CU_add_test(pSuite, "register_command_no_apikey_fail", register_command_no_apikey_fail)) ||
          (NULL == CU_add_test(pSuite, "register_command_solver_args", register_command_solver_args)) ||
          (NULL == CU_add_test(pSuite, "register_command_solver_args_flags", register_command_solver_args_flags)) ||
          (NULL == CU_add_test(pSuite, "register_command_solver_args_nothreads", register_command_solver_args_nothreads))
      ) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Run all tests using the CUnit Basic interface */
  CU_basic_set_mode(CU_BRM_VERBOSE);
  CU_basic_run_tests();
  CU_cleanup_registry();
  return CU_get_error();
}
