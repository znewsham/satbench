#include "formula.h"
#include "main.h"
#include <SATBench.h>
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <string.h>
 #include <openssl/sha.h>
#include "sha256_file.h"
int f_sha256(unsigned char* dest, char* filename){
  FILE* file;
  int read;
  int BUFFER_LENGTH = 4096*16;
  char buffer[BUFFER_LENGTH];
  unsigned int length;
  file = fopen(filename, "r");
  if(file <= 0){
    printf("%s\n", filename);
    perror("No file");
    return -1;
  }
  SHA256_CTX sha256;
  SHA256_Init(&sha256);
  int bytesRead = 0;
  if(!buffer) return -1;
  while((bytesRead = fread(buffer, 1, BUFFER_LENGTH, file)))
  {
      SHA256_Update(&sha256, buffer, bytesRead);
  }
  SHA256_Final(dest, &sha256);

  /*
   * EVP_MD_CTX *mdctx;
  const EVP_MD *md;
  mdctx = EVP_MD_CTX_create();
  length = 0;
  OpenSSL_add_all_digests();
  md = EVP_get_digestbyname("sha256");
  EVP_DigestInit_ex(mdctx, md, NULL);
  while((read = fread((void*)buffer, 1, BUFFER_LENGTH, file)) != 0){
    EVP_DigestUpdate(mdctx, buffer, read);
    length += read;
    if(read < BUFFER_LENGTH){
      break;
    }
  }
  EVP_DigestFinal_ex(mdctx, dest, &length);
  EVP_MD_CTX_destroy(mdctx);
  EVP_cleanup();*/
  return 0;
}

int register_formula(int argsc, char** argsv){
  int c = 0;
  int i;
  int option_index = 0;
  char filename[512];
  char filename2[512];
  int length;
  struct option *sel;
  
  int num_args = 0;
  int need_value = 0;
  char *keys[32];
  char *values[32];
  char *start;
  strcpy(apikey, "");
  optind = 1;
  struct SATBench_formula formula;
   while (1){
      c = getopt_long (argsc, argsv, options_string[FORMULA_INDEX],formula_options, &option_index);
      if (c == -1){
        break;
      }
      switch(c){
        case 'a':
          strcpy(apikey, optarg);
          break;
        case 'b':
          if(strlen(optarg) > 512){
            fprintf(stderr, "Filename too long (512)");
            return -1;
          }
          strcpy(filename, optarg);
          strcpy(filename2, optarg);
          name_from_file(formula.formula_name, filename2);
          break;
        case 'c':
          if(strlen(optarg) > 255){
            fprintf(stderr, "Name too long (255)");
            return -1;
          }
          strcpy(formula.formula_name, optarg);
          break;
        case 'd':
        case 'e':
          sel = option_from_char(c, formula_options); 
          if(sel != 0){
            if(strlen(optarg) > 128){
              fprintf(stderr, "%s too long (128)", sel->name);
              return -1;
            }
            keys[num_args] = malloc(32);
            values[num_args] = malloc(128);
            strcpy(keys[num_args], sel->name);
            strcpy(values[num_args], optarg);
            num_args++;
          }
          break;
        case 'f':
          if(strlen(optarg) > 2048){
            fprintf(stderr, "Options too long (2048)");
            return -1;
          }
          start= optarg;
          for(i = 0; i < strlen(start); i++){
            if(start[i] == '='){
              need_value=1;
              keys[num_args] = malloc(32);
              bzero(keys[num_args],32);
              if(strlen(start) > 32){
                fprintf(stderr, "%s name too long (32)", start);
                return -1;
              }
              strncpy(keys[num_args], start, i);
              start = &start[i + 1];
              i=0;
            }
            if(start[i] == ','){
              need_value=0;
              values[num_args] = malloc(128);
              bzero(values[num_args],128);
              if(strlen(start) > 128){
                fprintf(stderr, "%s value too long (128)", start);
                return -1;
              }
              strncpy(values[num_args], start, i);
              start = &start[i + 1];
              i=0;
              num_args++;
            }
          }
          if(need_value){
            values[num_args] = malloc(128);
              if(strlen(start) > 128){
                fprintf(stderr, "%s value too long (128)", start);
                return -1;
              }
            strncpy(values[num_args], start, i);
            num_args++;
          }
        case 0:
        case '?':
          break;
        default:
          printf("%c\n", c);
          help();
          abort();
      }
   }
  formula.simplified = simp_flag;
  keys[num_args] = malloc(32);
  values[num_args] = malloc(128);
  keys[num_args + 1] = malloc(32);
  values[num_args + 1] = malloc(128);
  keys[num_args + 2] = malloc(32);
  values[num_args + 2] = malloc(128);
  if(app_flag != -1){
    strcpy(keys[num_args], "app\0");
    strcpy(values[num_args], app_flag ? "1\0" : "0\0");
    num_args++;
  }
  if(hard_flag != -1){
    strcpy(keys[num_args], "hard\0");
    strcpy(values[num_args], hard_flag ? "1\0" : "0\0");
    num_args++;
  }
  if(random_flag != -1){
    strcpy(keys[num_args], "random\0");
    strcpy(values[num_args], random_flag ? "1\0" : "0\0");
    num_args++;
  }
  formula.keys = keys;
  formula.values = values;
  formula.length = num_args;
  if(f_sha256(formula.sha256, filename) == -1){
    return -1;
  }
  if(strcmp(apikey,"") == 0){
    fprintf(stderr, "No API key defined\n");
    return -1;
  }
  SATBench_register_formula(apikey, &formula);
  
  for(i = 0; i < SATBench_numwarnings; i++){
    fprintf(stderr, "%s\n", SATBench_warnings[i]);
  }
  return formula.formula_id;
}

