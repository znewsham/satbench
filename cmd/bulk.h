/* 
 * File:   bulk.h
 * Author: zacknewsham
 *
 * Created on October 16, 2014, 2:22 AM
 */

#include <getopt.h>
#include "main.h"
#ifndef BULK_H
#define	BULK_H

#ifdef	__cplusplus
extern "C" {
#endif
static char* bulk_types[] = {"string(128)\0","string(512)\0","int\0","int\0","string(32)\0","string(32)\0","string(32)\0","string(32)\0","string(512)\0","string(512)\0"};

static struct option bulk_options[] = 
{//command options
  {"apikey",  required_argument, 0, 'a'},
  {"file",  required_argument, 0, 'b'},
  {"worker",  required_argument, 0, 'c'},
  {"command",  required_argument, 0, 'd'},
  {"filefield",  required_argument, 0, 'e'},
  {"wcfield",  optional_argument, 0, 'f'},
  {"cpufield",  optional_argument, 0, 'g'},
  {"resultfield",  optional_argument, 0, 'h'},
  {"formulafields",  optional_argument, 0, 'i'},
  {"trialfields",  optional_argument, 0, 'j'},
  {0, 0, 0, 0}
};
int register_bulk(int argsc, char** argsv);



#ifdef	__cplusplus
}
#endif

#endif	/* BULK_H */

