#include "bulk.h"
#include "main.h"
#include "formula.h"
#include <SATBench.h>
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <openssl/evp.h>
int _sha256(unsigned char* dest, char* filename){
  FILE* file;
  int read;
  int BUFFER_LENGTH = 1024;
  char buffer[BUFFER_LENGTH];
  EVP_MD_CTX *mdctx;
  const EVP_MD *md;
  unsigned int length;
  file = fopen(filename, "r");
  if(file <= 0){
    printf("%s\n", filename);
    perror("No file");
    return -1;
  }
  mdctx = EVP_MD_CTX_create();
  length = 0;
  OpenSSL_add_all_digests();
  md = EVP_get_digestbyname("sha256");
  EVP_DigestInit_ex(mdctx, md, NULL);
  while((read = fread((void*)buffer, 1, BUFFER_LENGTH, file)) != 0){
    EVP_DigestUpdate(mdctx, buffer, read);
    length += read;
    if(read < BUFFER_LENGTH){
      break;
    }
  }
  EVP_DigestFinal_ex(mdctx, dest, &length);
  EVP_MD_CTX_destroy(mdctx);
  EVP_cleanup();
  return 0;
}

int register_bulk(int argsc, char** argsv){
  char file[512];
  char filefield[32];
  char filename[512];
  char wctimefield[32];
  char cputimefield[32];
  char resultfield[32];
  char formulafields[64][128];
  char trialfields[32][128];
  strcpy(file,"");
  strcpy(filename,"");
  strcpy(filefield,"");
  strcpy(wctimefield,"");
  strcpy(cputimefield,"");
  strcpy(resultfield,"");
  int worker_id = 0;
  int command_id = 0;
  int c = 0;
  int i;
  int option_index = 0;
  int num_fields = 0;
  int num_trial_fields = 0;
  
  struct SATBench_formula formula;
  bzero(&formula,sizeof(formula));
  bzero(formulafields, 64*128);
  formula.keys = malloc(64 * sizeof(char*));
  formula.values = malloc(64 * sizeof(char*));
  struct SATBench_trial trial;
  bzero(&trial,sizeof(trial));
  strcpy(apikey, "");
  while (1){
      c = getopt_long (argsc, argsv, options_string[BULK_INDEX],bulk_options, &option_index);
      if (c == -1){
        break;
      }
      char *start;
      switch(c){
        case 'a':
          strcpy(apikey, optarg);
          break;
        case 'b':
          strcpy(filename, optarg);
          break;
        case 'c':
          worker_id=atoi(optarg);
          break;
        case 'd':
          command_id=atoi(optarg);
          break;
        case 'e':
          strcpy(filefield, optarg);
          break;
        case 'f':
          strcpy(wctimefield, optarg);
          break;
        case 'g':
          strcpy(cputimefield, optarg);
          break;
        case 'h':
          strcpy(resultfield, optarg);
          break;
        case 'i':
          start= optarg;
          for(i = 0; i < strlen(start); i++){
            if(start[i] == ','){
              strncpy(formulafields[num_fields], start, i);
              start = &start[i + 1];
              i=0;
              if(strcmp(formulafields[num_fields],"") != 0){
                num_fields++;
              }
            }        
          }
          strncpy(formulafields[num_fields], start, i);
          num_fields++;
          break;
        case 'j':
          start= optarg;
          for(i = 0; i < strlen(start); i++){
            if(start[i] == ','){
              strncpy(trialfields[num_trial_fields], start, i);
              start = &start[i + 1];
              i=0;
              num_trial_fields++;
            }
          }
          break;
      }
  }
  
  if(strcmp(apikey,"") == 0){
    fprintf(stderr, "No API key defined\n");
    return -1;
  }
  if(strcmp(filefield,"") == 0){
    fprintf(stderr, "No file field provided\n");
    return -1;
  }
  if(strcmp(filename,"") == 0){
    fprintf(stderr, "No input file provided\n");
    return -1;
  }
  
  if(strcmp(wctimefield,"") == 0 && strcmp(cputimefield,"") == 0){
    fprintf(stderr,"Must either include wcfield or cpufield\n");
  }
  else{
    if(worker_id == 0){
      fprintf(stderr, "No worker defined\n");
      return -1;
    }
    if(command_id == 0){
      fprintf(stderr, "No command defined\n");
      return -1;
    }
  }
  
  trial.worker_id=worker_id;
  trial.command_id=command_id;
  
  FILE* csv = fopen(filename,"r");
  if(csv <= 0){
    perror("File doesnt exist");
    return -1;
  }
  char line [1024];
  int first = 1;
  int column = 0;
  int formula_columns[64];
  for(i = 0; i < 64; i++){
    formula_columns[i] = -1;
  }
  int trial_columns[32];
  for(i = 0; i < 32; i++){
    trial_columns[i] = -1;
  }
  int a;
  char *current = malloc(512);
  int wctimecolumn = -1;
  int cputimecolumn = -1;
  int resultcolumn = -1;
  int filecolumn = -1;
  char * startCurrent = current;
  while ( !feof(csv) && fgets ( line, sizeof line, csv ) != NULL ){
    current = startCurrent;
    int args = 0;
    char* start = line;
    column = 0;
    if(first){
      for(i = 0; i < strlen(start); i++){
        if(start[i] != ',' && start[i] != '\n'){
          continue;
        }
        strncpy(current, start, i);
        current[i] = '\0';
        if(current[0] == '"'){
          current = &current[1];
          if(current[i - 2] == '"'){
            current[i - 2] = '\0';
          }
        }

        start=&start[i + 1];
        i = 0;
        if(strcmp(current, wctimefield) == 0){
          wctimecolumn = column;
        }
        if(strcmp(current, cputimefield) == 0){
          cputimecolumn = column;
        }
        if(strcmp(current, resultfield) == 0){
          resultcolumn = column;
        }
        if(strcmp(current, filefield) == 0){
          filecolumn = column;
        }
        for(a = 0; a < num_fields; a++){
          if(strcmp(current, formulafields[a]) == 0){
            formula_columns[column] = a;
          }
        }
        for(a = 0; a < num_trial_fields; a++){
          if(strcmp(current, trialfields[a]) == 0){
            trial_columns[column] = a;
          }
        }
        column++;
      }
      first = 0;
      continue;
    }
    int ret = 0;
    for(i = 0; i < strlen(start); i++){
      int finish = 0;
      if(start[i] != ',' && start[i] != '\n' && start[i] != '\0'){
        continue;
      }
      if(start[i] != ','){
        finish = 1;
      }
      strncpy(current, start, i);
      current[i] = '\0';
      if(current[0] == '"'){
        current = &current[1];
        if(current[i - 2] == '"'){
          current[i - 2] = '\0';
        }
      }
      start=&start[i + 1];
      i = 0;
      if(column == filecolumn){
        ret = _sha256(formula.sha256, current);
        if(ret == -1){
          break;
        }
        char name[256];
        strcpy(name, current);
        name_from_file(formula.formula_name, name);
      }
      else if(column == wctimecolumn){
        trial.wc_time_millis=atof(current);
      }
      else if(column == cputimecolumn){
        trial.cpu_time_millis=atof(current);
      }
      else if(column == resultcolumn){
        trial.result=atoi(current);
      }
      else if(column < 64 && formula_columns[column] != -1){
        formula.keys[args] = malloc(128);
        strcpy(formula.keys[args], formulafields[formula_columns[column]]);
        formula.values[args] = malloc(256);
        if(strcmp(current, "TRUE") == 0){
          strcpy(formula.values[args], "1");
        }
        else if(strcmp(current, "FALSE") == 0){
          strcpy(formula.values[args], "0");
        }
        else{
          strcpy(formula.values[args], current);
        }
        args++;
      }
      if(finish){
        break;
      }
      column++;

    }
    if(ret == -1){
      continue;
    }
    formula.length = args;
    SATBench_register_formula(apikey, &formula);
    for(i = 0; i < args; i++){
      free(formula.keys[i]);
      free(formula.values[i]);
    }
    free(SATBench_warnings);
    if(strcmp(wctimefield,"") != 0 || strcmp(cputimefield, "") != 0){
      trial.formula_id=formula.formula_id;
      SATBench_register_trial(apikey, &trial);
    }
    printf("F:%d T:%d\n", formula.formula_id, trial.trial_id);
  }
  free(formula.keys);
  free(formula.values);
  free(current);
  //fclose ( csv );
  return 1;
}