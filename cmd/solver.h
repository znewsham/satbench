/* 
 * File:   solver.h
 * Author: zacknewsham
 *
 * Created on October 16, 2014, 2:22 AM
 */

#include <getopt.h>
#include "main.h"
#ifndef SOLVER_H
#define	SOLVER_H

#ifdef	__cplusplus
extern "C" {
#endif

static char* solver_types[] = {"string(128)\0","string(512)\0","string(255)\0","string(255)\0"};

static struct option solver_options[] = 
{ //solver options
  {"apikey",  required_argument, 0, 'a'},
  {"file",  required_argument, 0, 'b'},
  {"name",  optional_argument, 0, 'c'},
  {"version",  required_argument, 0, 'd'},
  {0, 0, 0, 0}
};
int register_solver(int argsc, char** argsv);




#ifdef	__cplusplus
}
#endif

#endif	/* SOLVER_H */

