/* 
 * File:   command.h
 * Author: zacknewsham
 *
 * Created on October 16, 2014, 2:20 AM
 */

#include <getopt.h>
#include "main.h"
#ifndef COMMAND_H
#define	COMMAND_H

#ifdef	__cplusplus
extern "C" {
#endif
static char* command_types[] = {"string(128)\0","int\0","string(2048)\0","int\0","\0","\0"};


static struct option command_options[] = 
{//command options
  {"apikey",  required_argument, 0, 'a'},
  {"solver",  required_argument, 0, 'b'},
  {"args",  required_argument, 0, 'c'},
  {"threads",  optional_argument, 0, 'd'},
  {"simp",  no_argument, &simp_flag, 1},
  {"noout",  no_argument, &noout_flag, 1},
  {0, 0, 0, 0}
};
int register_command(int argsc, char** argsv);



#ifdef	__cplusplus
}
#endif

#endif	/* COMMAND_H */

