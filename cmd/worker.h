/* 
 * File:   worker.h
 * Author: zacknewsham
 *
 * Created on October 16, 2014, 2:22 AM
 */

#include <getopt.h>
#include "main.h"
#ifndef WORKER_H
#define	WORKER_H

#ifdef	__cplusplus
extern "C" {
#endif

static char* worker_types[] = {
  "string(128)\0",
  "string(255)\0",
  "int\0",
  "string(16)\0",
  "string(128)\0", 
  "string(128)\0", 
  "int\0", 
  "int\0", 
  "int\0", 
  "int\0", "int\0", 
  "double\0", 
  "int\0", 
  "int\0", 
  "int\0", 
  "int\0"};

static struct option worker_options[] = 
{ //worker options
  {"apikey",  required_argument, 0, 'a'},
  {"name",  required_argument, 0, 'b'},
  {"cpu-speed",  required_argument, 0, 'c'},
  {"cpu-arch",  required_argument, 0, 'd'},
  {"cpu-manufacturer",  required_argument, 0, 'e'},
  {"cpu-model",  required_argument, 0, 'f'},
  {"cpu-cores",  required_argument, 0, 'g'},
  {"l1-i-kbytes",  optional_argument, 0, 'h'},
  {"l1-d-kbytes",  optional_argument, 0, 'i'},
  {"l2-kbytes",  optional_argument, 0, 'j'},
  {"l3-kbytes",  optional_argument, 0, 'k'},
  {"ram-kbytes",  required_argument, 0, 'l'},
  {"ram-speed",  optional_argument, 0, 'm'},
  {"ram-channels",  optional_argument, 0, 'n'},
  {"page-kbytes",  optional_argument, 0, 'o'},
  {"storage-speed",  optional_argument, 0, 'p'},
  {"storage-connection-mbytes",  optional_argument, 0, 'q'},
  {0, 0, 0, 0}
};
int register_worker(int argsc, char** argsv);


#ifdef	__cplusplus
}
#endif

#endif	/* WORKER_H */

