#include "worker.h"
#include "main.h"
#include <SATBench.h>
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <openssl/evp.h>
int register_worker(int argsc, char** argsv){
  int c = 0;
  int option_index = 0;
  struct SATBench_worker worker;
  struct SATBench_worker workers[5];
  bzero(&worker, sizeof(struct SATBench_worker));
  strcpy(apikey,"");
  while (1) {
      c = getopt_long (argsc, argsv, options_string[WORKER_INDEX],worker_options, &option_index);
      if (c == -1){
        break;
      }
      switch(c){
        case 'a':
          strcpy(apikey, optarg);
          break;
        case 'b':
          if(strlen(optarg) > 255){
            fprintf(stderr, "Worker too long (255)\n");
            return -1;
          }
          strcpy(worker.worker_name, optarg);
          break;
        case 'c':
          worker.cpu_speed = atoi(optarg);
          break;
        case 'd':
          if(strlen(optarg) > 16){
            fprintf(stderr, "CPU Arch too long (16)\n");
            return -1;
          }
          strcpy(worker.cpu_arch, optarg);
          break;
        case 'e':
          if(strlen(optarg) > 255){
            fprintf(stderr, "CPU Manufacturer too long (128)\n");
            return -1;
          }
          strcpy(worker.cpu_manufacturer, optarg);
          break;
        case 'f':
          if(strlen(optarg) > 255){
            fprintf(stderr, "CPU Model too long (128)\n");
            return -1;
          }
          strcpy(worker.cpu_model, optarg);
          break;
        case 'g':
          worker.cpu_cores = atoi(optarg);
          break;
        case 'h':
          worker.l1_i_kbytes = atoi(optarg);
          break;
        case 'i':
          worker.l1_d_kbytes = atoi(optarg);
          break;
        case 'j':
          worker.l2_kbytes = atoi(optarg);
          break;
        case 'k':
          worker.l3_kbytes = atoi(optarg);
          break;
        case 'l':
          worker.ram_kbytes = atoi(optarg);
          break;
        case 'm':
          worker.ram_speed = atof(optarg);
          break;
        case 'n':
          worker.ram_channels = atoi(optarg);
          break;
        case 'o':
          worker.page_kbytes = atoi(optarg);
          break;
        case 'p':
          worker.storage_speed = atoi(optarg);
          break;
        case 'q':
          worker.storage_connection_mbytes = atoi(optarg);
          break;
        case 0:
        case '?':
          break;
        default:
          help();
          abort();
      }
   }
  
  if(strcmp(apikey,"") == 0){
    fprintf(stderr, "No API key defined\n");
    return -1;
  }
  if(strcmp(worker.worker_name,"") == 0){
    fprintf(stderr, "No Worker defined\n");
    return -1;
  }
  if(worker.cpu_speed == 0){
    fprintf(stderr, "No CPU speed (Mhz) defined\n");
    return -1;
  }
  if(strcmp(worker.cpu_arch,"") == 0){
    fprintf(stderr, "No CPU Arch defined\n");
    return -1;
  }
  if(strcmp(worker.cpu_model,"") == 0){
    fprintf(stderr, "No CPU Model defined\n");
    return -1;
  }
  if(strcmp(worker.cpu_manufacturer,"") == 0){
    fprintf(stderr, "No CPU Manufacturer defined\n");
    return -1;
  }
  else if(strlen(worker.cpu_manufacturer) > 128){
    fprintf(stderr, "CPU Manufacturer too long (128)\n");
    return -1;
  }
  if(worker.cpu_cores == 0){
    fprintf(stderr, "No CPU cores defined\n");
    return -1;
  }
  if(worker.ram_kbytes == 0){
    fprintf(stderr, "No RAM Size (kbytes) defined\n");
    return -1;
  }
  
  SATBench_register_worker(apikey, &worker);
  return worker.worker_id;
}